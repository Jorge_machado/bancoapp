﻿using BancoApp_Shared;
using Microsoft.EntityFrameworkCore;
using System.Diagnostics.Metrics;
using System.Xml;

namespace BancoApp_Api.Data
{
    public class DataContext : DbContext
    {

        public DataContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Usuario> Usuario { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

        //    //Creacion de index
                modelBuilder.Entity<Usuario>().HasIndex(x => x.PrimerNombre).IsUnique();
        //    //modelBuilder.Entity<State>().HasIndex("CountryId", "Name").IsUnique();

        //    //modelBuilder.Entity<Product>().HasIndex(p => p.Name).IsUnique();
        }
    }
}
