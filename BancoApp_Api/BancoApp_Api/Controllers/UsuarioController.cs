﻿using BancoApp_Api.Data;
using BancoApp_Shared;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace BancoApp_Api.Controllers
{
    [Route("api/usuarios")]
    [ApiController]
    public class UsuarioController : ControllerBase
    {
        private readonly DataContext _dbContext;

        public UsuarioController(DataContext dbContext)
        {
            _dbContext = dbContext;
        }


        [HttpGet]
        public async Task<ActionResult> Get([FromQuery] Usuario usuario)
        {
            var queryable = _dbContext.Usuario
                .AsQueryable();

            return Ok(await queryable
                .OrderBy(x => x.PrimerNombre)
                .ToListAsync());
        }



        // GET api/<Usuario>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<Usuario>
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/<Usuario>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<Usuario>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
