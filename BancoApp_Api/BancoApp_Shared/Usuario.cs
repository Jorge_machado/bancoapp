﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BancoApp_Shared
{
    public class Usuario
    {
        [Key]
        public int IdUsuario { get; set; }
        public long IdentificacionUsuario { get; set; }
        public int IdTipoIdentificacion { get; set; }
        public string? PrimerNombre { get; set; }
        public string? SegundoNombre { get; set; }
        public string? PrimerApellido { get; set; }
        public string? SegundoApellido { get; set; }
        public string? Telefono { get; set; }
        public string? Email { get; set; }
        public string? Direccion { get; set; }
        public int IdPais { get; set; }
        public int IdDepartamento { get; set; }
        public int IdCiudad { get; set; }
        public string? FotoUsuario { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime FechaModificacion { get; set; }
    }
}
